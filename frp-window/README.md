#使用说明
比方我的阿里云服务器ip是81.70.39.12
防火墙放开的公网端口是7000
我本地IP是127.0.0.1 对外提供端口是8081
解压命令 
```
#解压
tar -xvzf frp_0.43.0_linux_amd64.tar.gz
重命名
mv frp_0.43.0_linux_amd64 frp
```
解压后的文件
服务端 frps 
客户端 frpc

###配置参考如下
1.服务端配置 frps.ini
```
[common]
#服务端监听ip 正常都是0
bind_addr = 0.0.0.0
#提供服务的服务端口
bind_port = 7000
privilege_token = 12345678

```
服务端启动命令 
```
./frps -c frps.ini
```
2.客户端配置 frpc.ini
```
[common]
#服务端ip
server_addr = 81.70.39.12
#服务端提供服务的端口
server_port = 7000
privilege_token = 12345678
[web]
type = tcp
#本地ip
local_ip = 127.0.0.1
#本地端口
local_port = 8081
#监听服务端的端口 
remote_port = 8081

```
客户端启动命令 D:\下载\frp>frpc.exe -c frpc.ini

通过以上配置  访问81.70.39.12:8081会转发到本地127.0.0.1:8081
公网的8081如果不打算放开防火墙，还可以通过nginx配置域名转发到服务器的127.0.0.1:8081 即可